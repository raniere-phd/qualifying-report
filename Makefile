.PHONY: _book final references-from-zotero.bib 3d img

ALL_RMD := $(wildcard *.Rmd)

all: html html_book

html: report.html slides.html

html_book: _book

pdf: report.pdf

final: silva-raniere.pdf

silva-raniere.pdf: cover.pdf report.pdf
	pdftk $^ output $@

cover.pdf: cover.odt
	libreoffice --convert-to pdf $<

_book: $(ALL_RMD) references-from-zotero.bib 3d img
	Rscript -e 'bookdown::render_book(".", "bookdown::gitbook")'

report.html: $(ALL_RMD) references-from-zotero.bib 3d img
	Rscript -e 'bookdown::render_book(".", "bookdown::html_document2")'

report.pdf: $(ALL_RMD) references-from-zotero.bib 3d img
	Rscript -e 'bookdown::render_book(".", "bookdown::pdf_book")' && mv _book/report.pdf $@

slides.html: slides.md slides.css
	pandoc \
	--from markdown \
	--to revealjs \
	-s \
	-V controls=false \
	-V transition=none \
	-V center=false \
	-V theme=white \
	--css slides.css \
	--output $@ \
	$<

3d:
	$(MAKE) -C $@

img:
	$(MAKE) -C $@

references-from-zotero.bib:
	@echo 'Use Better BibTeX for Zotero <https://retorque.re/zotero-better-bibtex> to keep keys updated.'
