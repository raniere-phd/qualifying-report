# Radiograph

In 1785,
X-rays were first mentioned in the scientific literature
by @morganXIVElectricalExperiments1785.
In 1895,
the first radiograph was created
by @rontgenNewKindRays1970
and
the term X-rays appeared for the first time.
To create a radiograph,
the object of interest is placed between
the source of X-rays
and
the film cassette (for traditional radiography)
or imaging plate (for digital radiography)\ [@thrallTextbookVeterinaryDiagnostic2017],
see Figure\ \@ref(fig:x-rays-diagram).
In traditional radiograph,
the part of the film that was exposed to X-rays appears black after the developing process\ [@thrallTextbookVeterinaryDiagnostic2017].
Digital radiography acquisition hardware
are of two types:
computed radiography 
and
direct digital radiography\ [@thrallTextbookVeterinaryDiagnostic2017].
In digital radiography,
X-rays create visible light or electrical charges
when striking the detector
and the sensor reports which parts of it were activated by visible light or electrical charges
to a computer that stores the information in a digital file
for future visualisation\ [@thrallTextbookVeterinaryDiagnostic2017]
--
digital visualisation mimics traditional radiography,
black will be use to represent areas exposed to X-rays.
Today,
the majority of radiography is digital
and
stored as Digital Imaging and Communications in Medicine (DICOM) files\ [@nationalelectricalmanufacturersassociationNEMAPS3ISO2021].

(ref:x-rays-diagram) Illustration source of X-rays, object of interest, and imaging plate. (a) X-ray is absorbed by the object of interest. (b) X-ray reaches the imaging plate.

```{r x-rays-diagram, echo = FALSE, fig.cap = "(ref:x-rays-diagram)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-diagram.png'
)
```

Radiograph is a projection of a three dimensional object into a two dimensional canvas.
Because of the dimensional reduction,
two different objects may have the same radiograph,
see Figure\ \@ref(fig:x-rays-twins).
Other limitations due the physics that make radiographs possible are
magnification,
distortion,
image of a familiar part appearing unfamiliar,
loss of volumetric perception,
and
superimposition\ [@thrallTextbookVeterinaryDiagnostic2017].

(ref:x-rays-twins) Illustration of "twin" radiographs. (a) Radiograph from half of a cube. (b) Radiograph from a cube. (c) Radiograph from a pyramid.

```{r x-rays-twins, echo = FALSE, fig.cap = "(ref:x-rays-twins)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-twins.png'
)
```

Magnification refers to the ratio of object size to the projection size,
see Figure\ \@ref(fig:x-rays-magnification).
X-ray photons,
like visible light,
travel only in straight lines
and
a object closer to the source of X-rays will have a bigger projection/shadow
than a object far from the source of X-rays.

(ref:x-rays-magnification) Illustration of magnification in radiographs. (a) Projection is bigger than object because object is closer to the source of X-rays. (b) Projection and object are the size because object is in the "optimal" position. (a) Projection is smaller than object because object is far from the source of X-rays.

```{r x-rays-magnification, echo = FALSE, fig.cap = "(ref:x-rays-magnification)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-magnification.png'
)
```

Distortion refers to unequal magnification of object
because object and receiver planes are not parallel,
see Figure\ \@ref(fig:x-rays-distortion).
Unfamiliar image is the result of many distortions
because the patient is not oriented in a standard manner.

(ref:x-rays-distortion) Illustration of distortion in radiographs. (a) Projection and object are the size because object and receiver planes are parallel. (b) Distorted projection due unequal maginification of object.

```{r x-rays-distortion, echo = FALSE, fig.cap = "(ref:x-rays-distortion)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-distortion.png'
)
```

Loss of volumetric perception refers to the inability to identify
what part of the object is closer to the source of X-rays
and
what part is far.
One way to recover the volumetric perception
is to use two radiographs,
called orthogonal views
or
orthogonal projections,
with one acquired at a 90-degree angle to the other,
see Figure\ \@ref(fig:x-rays-volume).

(ref:x-rays-volume) Illustration of loss of volumetric perception, superimposition and orthogonal views. (a) The object is composed of two cubes but only one is visible and is impossible to tell which cube is closer to the source of X-rays. (b) Orthogonal view that aid the interpretation by informing which cube is closer to the source of X-rays.

```{r x-rays-volume, echo = FALSE, fig.cap = "(ref:x-rays-volume)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-volume.png'
)
```

Superimposition of one structure on another,
see Figure\ \@ref(fig:x-rays-volume)\ (a),
are common in every radiograph
and may lead to misinterpretation.
Issues from superimposition are mitigated by the use of two or more radiographs.

## Blackness and Opacity

Consider a very small object
composed of one hundred atoms of a single chemical element
and
the radiograph of this object.
The degree of blackness because of the object in the radiograph
is affected by the number of X-rays reaching that area,
see Figure \@ref(fig:x-rays-blackness),
which is a function of
the chemical element's atomic number,
object's thickness,
amount of time of exposure (mAs),
energy of the X-ray beam (kVp),
and the distance from the X-ray tube to the sensor (FFD).

(ref:x-rays-blackness) Illustration of different blackness and their cause. The value of the parameter grows from the left to the right.

```{r x-rays-blackness, echo = FALSE, fig.cap = "(ref:x-rays-blackness)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-blackness.png'
)
```

When interpreting the radiograph of a body part,
the chemical element's atomic number is naively replaced by "density",
see Figure \@ref(fig:x-rays-blackness-body),
and
density,
thickness,
amount of time of exposure,
energy of the X-ray beam,
and
distance from the X-ray tube
must be considered careful,
see Table\ \@ref(tab:equine-exposure).

(ref:x-rays-blackness-body) Illustration of different blackness and the their standard interpretation.

```{r x-rays-blackness-body, echo = FALSE, fig.cap = "(ref:x-rays-blackness-body)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-blackness-body.png'
)
```


```{r warning=FALSE, include=FALSE}
equine_exposure <- read_csv('data/equine-exposure.csv')
```

(ref:equine-exposure) Exposure chart for equines: young animal (YA), medium-sized animal (MA), and heavy animal (HA). Source: [@prasadImagingTechniquesVeterinary2021].

```{r equine-exposure, echo=FALSE, message=FALSE}
equine_exposure %>%
  knitr::kable(
    booktabs = TRUE,
    caption = '(ref:equine-exposure)',
    longtable=FALSE
)
```

## Factor Affecting Radiograph Detail and Contrast

The primary factor for low detail in radiographs
is motion
(from the object or the X-ray machine)
as it will change some of the variables related with the blackness
mentioned before.
By reducing the exposure time,
the change of object or X-ray machine move is reduced.
To compensate for the reducing exposure time,
it is required to increase the energy of the X-ray beam.

Another source of low detail in radiographs
is additional "X-ray" created when the primary X-ray interact with the object,
called Compton scattered photons.
Compton scattered photons can be "filtered" by the use of a device called a grid
which may need to be compensate by increasing the energy of the X-ray beam.

Contrast refers to the difference in blackness between structures in the image
and
is responsible for the structures be "visible".
High-contrast image is more useful for assessment of bone.

(ref:x-rays-contrast) Illustration of (a) high-contrast and (b) low-contrast radiograph. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r x-rays-contrast, echo = FALSE, fig.cap = "(ref:x-rays-contrast)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-contrast.png'
)
```

## Digital Radiograph

Digital radiograph benefits of improvements in hardware and software
that enable
contrast optimization,
exposure latitude,
and
image postprocessing\ [@thrallTextbookVeterinaryDiagnostic2017].
Contrast optimization refers to software that
"correct" the opacity differences associated with differences in thickness.
Exposure latitude refers to software  that
"correct" the blackness based in the kVp and mAs values used.
Image postprocessing refers to the possibility of
change blackness, contrast, and magnification
with DICOM viewing software.

Digital images are two dimensional canvas filled with small squares called pixels,
see Figure\ \@ref(fig:x-rays-pixels).
In general,
a image with more pixels is consider better
but will require more storage space and computational resources (memory and CPU time).
The image can be of various types
and
the type will dictate what colours the canvas supports,
for example
8-bit supports $2^8 = 256$ shades of gray
and
RGB supports three channels (red, green and blue) of $256$ values.
Radiographs usually are 16-bit
and
support $2^{16} = 65536$ shades of gray.

(ref:x-rays-pixels) Illustration of (a) digital radiograph with annotation in red of region to be magnified, (b) close up in the pixels with annotation in red of region to be "magnified", and (c) pixel value as decimal of "maginified" region where 0 and 1 represent, respectively, white and black. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r x-rays-pixels, echo = FALSE, fig.cap = "(ref:x-rays-pixels)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/x-ray-pixels.png'
)
```

As mentioned previously,
digital radiographs are stored as Digital Imaging and Communications in Medicine (DICOM) files\ [@nationalelectricalmanufacturersassociationNEMAPS3ISO2021].
DICOM files include,
in addition to the radiograph,
a set of information related to the radiographs,
for example
creation date,
anatomic regions,
manufacturer,
manufacturer's model,
operators' name,
volumetric properties,
acquisition contrast,
exposure time,
and
patient's name.
Because some of the information in the DICOM file is private,
is common practice to anonymise DICOM files when sharing them for research.
DICOM files can be manipulated with open source and commercial libraries and viewers.



