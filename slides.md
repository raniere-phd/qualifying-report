---
title: 'AI applied to racehorse radiograph'
author:
- Raniere Silva
date: 23-09-2021
title-slide-attributes:
  data-notes: 'Hello. My name is Raniere and welcome to my PhD Oral Qualifying Report. I will be talking about artificial intelligence applied to racehorse radiograph.'
---

## Introduction

![Photo of horse racing by Softeis used under the Creative Commons Attribution-Share Alike 3.0 Unported license.](img/commons.wikimedia.org/Horse-racing-1.jpg){height=9cm}

::: notes

The racehorse industry generated US$26 billion
and more than 500 thousands jobs
in Asia
during the 2018/2019 season.
Despite the economic impact of the racehorse industry,
the public perception of the sport
is diminished by the well-being of the horses,
especially by fractures during race days.
Reducing the number of fractures during race day
by adequate training schedules
and
identifying fractures in earlier stages
may improve the public perception of horse racing.
This study will focus in identifying fractures in earlier stages
with the assistance of artificial intelligence.

:::

## Horse Importation

![Illustration of horses' origin.](img/horse-importation.png)

::: notes

The Hong Kong Jockey Club imports all their horses from
<!-- Argentina, -->
Great Britain,
<!-- South Africa, -->
Australia,
<!-- Ireland,
United Arab Emirates,
Brazil,
Italy, -->
United States of America,
<!-- Canada, -->
Japan,
<!-- Chile, -->
New Zealand,
<!-- France,
Peru,
Germany,
Singapore. -->
and
other countries.

:::

## Pre-sale Radiographs

![Illustration of carpus, fetlock, tarsus, stifle and hoof location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.](img/all-examination-view-location.png)

::: notes

All horses considered for purchase
by The Hong Kong Jockey Club
undergo pre-sale examination,
a quality control procedure
that detects abnormalities
and estimates risk to horses, owners, and The Hong Kong Jockey Club.
48 standardised views
(covering carpus, fetlock, tarsus, stifle and hoof)
are specified by The Hong Kong Jockey Club
for the horse pre-sale examination.

At this point of the project,
the transfer of radiographs from the Hong Kong Jockey Club to CityU is in progress.

:::

## Radiograph

![Illustration source of X-rays, object of interest, and imaging plate.](img/x-ray-diagram.png)

::: notes

Radiograph is a projection of a three dimensional object into a two dimensional canvas.

To create a radiograph,
the object of interest is placed between
the source of X-rays
and
the film cassette (for traditional radiography)
or imaging plate (for digital radiography).

The degree of blackness because of the object in the radiograph
is affected by the number of X-rays reaching that area
which is a function of
the chemical element's atomic number,
object's thickness,
amount of time of exposure (mAs),
energy of the X-ray beam (kVp),
and the distance from the X-ray tube to the sensor (FFD).

:::

## Radiographs Acquisition

![Illustration of acquisition of digital radiograph. Image by [Karlyne](https://commons.wikimedia.org/w/index.php?title=User:Karlyne) used under the Creative Commons Attribution-Share Alike 3.0 Unported.](img/commons.wikimedia.org/radiograph-procedure.jpg)

::: notes

Equine radiographs are usually acquired using a portable X-ray machine like in the photo.

:::

## Blackness

![Illustration of different blackness and the their standard interpretation.](img/x-ray-blackness-body.png)

::: notes

The blackness can naively interpreted as indicated in the figure.

:::

## Twin Radiographs

![Illustration of “twin” radiographs. (a) Radiograph from half of a cube. (b) Radiograph from a cube. (c) Radiograph from a pyramid.](img/x-ray-twins.png)

::: notes

Let's consider some objects
made of an special material
that produce binary radiographs.

Because of the dimensional reduction in the radiographs,
half a cube,
cube,
and
pyramid
will have the same radiograph
if orientated like in the figure.

How to recover the volumetric perception?

:::

## Orthogonal Views

![Illustration of orthogonal views.](img/x-ray-volume.png)

::: notes

Orthogonal views are used to recover the volumetric perception.
Other limitations due the physics that make radiographs possible are
magnification,
distortion,
image of a familiar part appearing unfamiliar,
and
superimposition.

What abnormalities am I looking in the radiographs?

:::

## Fracture

![Annotated image showing a supracondylar fracture, red circle, with close up in the right. Case courtesy of Dr Mohamed Walaaedlin, Radiopaedia.org, [rID: 52307](https://radiopaedia.org/cases/52307) used under a modified Creative Commons Attribution-Non-commercial-Share Alike 3.0 Unported License.](img/radiographic-abnormality-fracture.png)

::: notes

Fracture
is a discontinuity in a bone.
Common causes of fractures are
trauma,
overuse (named fatigue fractures or stress fractures),
and
weak skeleton (named insufficiency fractures).

:::

## Fragment

![Annotated image showing fragment at the medial aspect of the talar dome, red circle, with close up in the right. Case courtesy of Dr Hani Makky Al Salam, Radiopaedia.org, [rID: 9247](https://radiopaedia.org/cases/9247) used under a modified Creative Commons Attribution-Non-commercial-Share Alike 3.0 Unported License.](img/radiographic-abnormality-fragment.png)

::: notes

Fragment
is a small separated segment of bone or cartilage
that may or may not be displaced.
Fragment is the result of traumatic injuries such as
fractures,
dislocations,
and
chondral injury.

:::

## Osteophyte

![Annotated image showing osteophyte at radial head and neck, red circle, with close up in the right. Case courtesy of Amanda Er, Radiopaedia.org, [rID: 85566](https://radiopaedia.org/cases/85566) used under a modified Creative Commons Attribution-Non-commercial-Share Alike 3.0 Unported License.](img/radiographic-abnormality-osteophyte.png)

::: notes

Osteophyte
is the formation of new bone on the surface of a bone,
also know as exostosis or bone spur,
that form along joint margins.
Common cause of osteophyte is
articular cartilage damage
due overuse.

How are digital radiographs stored?

:::

## Digital Radiograph

![Illustration of (a) digital radiograph with annotation in red of region to be magnified, (b) close up in the pixels with annotation in red of region to be "magnified", and (c) pixel value as decimal of "maginified" region where 0 and 1 represent, respectively, white and black. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.](img/x-ray-pixels.png)

::: notes

Digital images are two dimensional canvas filled with small squares called pixels.
In general,
a image with more pixels is consider better
but will require more storage space and computational resources (memory and CPU time).
The image can be of various types
and
the type will dictate what colours the canvas supports,
for example
8-bit supports $2^8 = 256$ shades of gray
and
RGB supports three channels (red, green and blue) of $256$ values.
Radiographs usually are 16-bit
and
support more than 50 thousands shades of gray.

Digital radiographs are stored as Digital Imaging and Communications in Medicine (DICOM) files.
DICOM files include,
in addition to the radiograph,
a set of information related to the radiographs,
for example
creation date,
anatomic regions,
manufacturer,
manufacturer's model,
operators' name,
volumetric properties,
acquisition contrast,
exposure time,
and
patient's name.
Because some of the information in the DICOM file is private,
is common practice to anonymise DICOM files when sharing them for research.

:::

## Machine Learning

![Illustration of relationship between machine learning and deep learning.](img_slides/machine-learning.png)

::: notes

Artificial intelligence
is the logic capacity that enables machines to predict the answer to a problem,
for example
reply to a given text
or
provide the name of a object in a photo.
Machine Learning is a group of artificial intelligence tools
based on statistical model
that finds features (also called patterns) in the data
during the training
and
uses the found features to make predictions.

Machine learning
can be subset in two groups:
supervised
and
unsupervised.

Supervised machine learning methods incorporate the outcomes
during training.

Unsupervised machine learning methods do not incorporate the outcomes
during training.

:::

## Deep Convolutional Neural Networks

![Illustration of neural network. (a) Input image. (b) Input layer. (c) Hidden layer. (d) Regression output layer. (e) Classification output layer. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.](img/neural-network.png)

::: notes

I will use Deep Convolutional Neural Networks
because it is the state of the art to classify images.

A neural network
has three layers:
input layer,
hidden layer,
and
output layer.
The input layer
is a placeholder for input data
and
it dictates
the shape of the input data,
for example 100x100 pixels.
Hidden layers is where the oracle lives.
The output layer
is responsible for converting the output of the hidden layer
into meaningful information,
for example the classification of the input.

Deep Convolutional Neural Networks is a group of neural networks
that is more than 10 layers deep
and
has a special type of layer named convolution among the hidden layers.

Before talk about convolution,
let's see how the hidden layers operate
if it has a single neuron.

:::

## Artificial Neuron's Prediction

![Diagram of artificial neuron. $x_1$, $x_2$, and $x_3$ are inputs. $w_1$, $w_2$, and $w_3$ are weights. $t$ is the critical threshold. $y'$ is the output produced.](img/neuron-computer-science.png)

::: notes

The artificial neuron
has weights associated with each dendrite
and
a critical threshold.
The artificial neuron
receives input thought each dendrite,
aggregates the inputs,
and
produces an output
based on the aggregate value and the critical threshold.

:::

## Training Artificial Neuron

![Illustration of logistic regression training. (a) Initial configuration of the problem. (b) Calculation of the prediction, $y^0$, for $x$. (c) Update of $w$ and $t$ based on the descent direction and step size previously calculated. (d) Final configuration.](img/logistic-regression-training.png)

::: notes

The logistic regression training
is an iterative method that takes
a set of points, $X$,
the classification of every point, $Y$,
a initial set of weights, $w^0$,
an activation function $\sigma$,
a initial critical threshold $t^0$,
and
a loss function (also called cost function) that gives the distance of the prediction $Y^i$ to $Y$.
At each iteration $i \geq 1$,
the following steps are executed:

1. calculate the prediction of every point, $Y^i$,
   based on $X$, $w^{i-1}$, $\sigma$, and $t^{i-1}$;
2. calculate a descent direction and step size
   based on the loss function for $Y^i$.
3. calculate $w^i$ and $t^i$
   based on the descent direction and step size.

:::

## Convolutional Layer

![Small scale example of convolutional layer. (a) Layer $l$. (b) Layer $l$ with zero padding size of 1. (c) First iteration of the convolution. (d) Second iteration of the convolution with stride size is 1.](img/neural-network-layer-convolutional-example.png)

::: notes

:::

## VGG-16

![Diagram of operations in VGG-16.](img/vggnet-16.png)

::: notes

VGG-16 is a deep convolutional neural networks
that examplify how layers are organised.

The VGG-16 starts with a RGB 224x224 image
and
ends with 512 7x7 features
before all feature are connected
and
reduced to the desired number of classes.

:::

## Challenge

![Annotated image showing triceps tendon enthesophyte, red circle, in different resolutions with close up in the right. Case courtesy of Dr Subhan Iqbal, Radiopaedia.org, [rID: 79944](https://radiopaedia.org/cases/79944) used under a modified Creative Commons Attribution-Non-commercial-Share Alike 3.0 Unported License.](img/input-size.png)

::: notes

One of the challenges related with AI applied to radiographs is the input size of the images
given that in small images the abnormalities might not be visible.

:::

## Planned Outputs

1. AI for Radiograph Classification
2. AI for Radiograph Abnormality Detection
3. AI for Risk Assessment

## AI for Radiograph Classification

![Illustration of AI for Radiograph Classification prediction.](img/ai-for-radiograph-classification.png)

::: notes

I will develop models
based on Inception\ v3
and
ResNet
using PyTorch library
to classify radiographs
into one of the $48$ standardised views.

The novelty of my work is to do the classification using grayscale images
between $48$ possible categories
when previous work are limited to $7$ possible categories.

I will also investigate the impact that
transfer learning have on my models
by pre-training the models with MURA.

Last,
I will investigate the prediction quality as a function of the input image size.

:::

## AI for Radiograph Abnormality Detection

![Illustration of AI for Radiograph Abnormality Detection prediction.](img/ai-for-radiograph-detection.png)

::: notes

I will train a new model
to classify abnormalities
using transfer learning from the model for radiograph classification.
To verify that the artificial intelligence
is "looking" at the correct portion of the image,
I will create a heat map
based on gradient-weighted class activation mapping
around the abnormality.

The novelty of this work is to use six body parts
(carpus,
front fetlock,
hind fetlock,
tarsus,
stifle,
and
front hoof)
and
more than one abnormality.

Also,
I will investigate the impact of a ensemble model that take into consideration,
like a radiologist,
different views of the same bone
to classify abnormalities.

:::

## AI for Risk Assessment

![Illustration of AI for Risk Assessment prediction.](img/ai-for-radiograph-risk-assessment.png)

::: notes

The third and last model to be develop
will extend AI for abnormality detection
by incorporating veterinary records
and
trackwork records.
The risk assessment generated by the model
will include
risk of the racehorse develop abnormality
and
likelihood of the racehorse reaches retirement.

:::

## Next Steps

1. Data acquisition
2. Model development
3. Model training
4. Model validation
5. Model testing

## Questions

![Photo by <a href="https://unsplash.com/@priscilladupreez?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Priscilla Du Preez</a> on <a href="https://unsplash.com/s/photos/horse-lego?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a> used under Unsplash License.](img_slides/unsplash.com/priscilla-du-preez-VPj1GNZ5WlA-unsplash.jpg)
  