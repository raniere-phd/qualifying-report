knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)

options(scipen=5)  # See https://stackoverflow.com/a/30889190 and https://stat.ethz.ch/pipermail/r-help//2009-November/411055.html


global_out_width <- if (knitr::is_latex_output()) "100%" else NULL